# TempoTapper

TempoTapper is a device, that can change the tempo clock using midi. It has been built to help Live Coders (particularly within FoxDot) to make music with live musicians, who do not want to follow the digital clock of software.

![Video can be found under /images](images/tempotapper.mp4)

<br>

| 1. Components      |
| :------------- |
| ![](images/tt_build_1.JPG) |

<br>

- 1 Arduino Micro (need to be an Arduino that works with MidiUSB)
- 1 x Piezo
- 1 x 1 MHz Resistor
- 1 x Small Developer Board
- 2 x 10 KHz Resistors
- Encoder
- Wires

<br>

| 2. Soldering     |
| :------------- |
| ![](images/tt_build_2.JPG) |

<br>

1. Arduino board >> socket pin strips
2. Arduino with strips >> Development Board
3. Encoder on Analog 3 >> Clock and Analog 5 to Data
4. Encoder Ground >> Arduino Ground
5. Clock >> 10 KHz resistor >> 5V Arduino
6. Data >> 10 KHz resistor >> 5V Arduino
7. Piezo Red + >> Arduino board Analog 0
8. Piezo Black - >> Arduino board Ground
9. 1 MHz Resistor between Piezo Red and Black

<br>

| 3. Case     |
| :------------- |
| ![](images/TempoTapper_CaseDesigns.png) |
|  |
| ![](images/tt_build_3.JPG) |

## How does it work?

- Upload the code to the TempoTapper
- Default bpm is 120
- The encoder changes the sensitivity of the tap receiver (piezo)
- Tap with the beat as long you need/want. Minimum is 3 times
- TempoTapper will get the average of all taps
- With the next tap, after the beat changed (indicated by LED), the midi message get send
- The TempoTapper will be recognized as Midi Device, so it can be connected to the software
