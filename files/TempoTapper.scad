//Design for TempoTapper Case
//Created by Jens Meisner
//License cc-by-sa 4.0


$fn=80;

height=3;
diameter=100;

show_part=2; //choose 1 or 2 or 3 for both

//Lower Part
if(show_part==1 || show_part==3)
{
    union()
    {
        translate([0,-diameter/2-10,height/2])
        cylinder(h=height, d=diameter, center=true);
        translate([0,0,height/2])
        cube([66, 46, height], center=true);
        difference()
        {
            union()
            {
                translate([0, 22, 8])
                rotate([90, 0, 0])
                cube([66, 16, 4], center=true);
                rotate([90, 0, 90])
                translate([0, 8, -31])
                cube([46, 16, 4], center=true);
                rotate([90, 0, 90])
                translate([0, 8, 31])
                cube([46, 16, 4], center=true);
                hull()
                {
                    translate([0,-diameter/2-10,3.5])
                    cylinder(h=height, d=38, center=true);  
                    translate([0, -24, 3.5])
                    cube([66, 2, 3], center=true);
                }
            }
            union()
            {
                translate([0,-2.5,height*3])
                cube([61, 48, 2], center=true);
                hull()
                {
                    translate([0,-diameter/2-5,height*1.5])
                    cylinder(h=3, d=28, center=true);
                    translate([0, -24, height*1.5])
                    cube([28, 2, 3], center=true);
                }
                translate([-25,-30,height/2])
                cylinder(h=height*3, d=2, center=true);
                translate([25,-30,height/2])
                cylinder(h=height*3, d=2, center=true);
                translate([0,-74,height/2])
                cylinder(h=height*3, d=2, center=true);
                rotate([90, 0, 90])
                translate([7, 13.5, 31])
                cube([12, 7, 4], center=true);
            }
        }
    }
}
//Upper Part
if(show_part==2 || show_part==3)
{
    union()
    {
        difference()
        {
            union()
            {
                translate([0, 0, 17.5])
                cube([72, 55, height], center=true);
                translate([0, 26, 14.5])
                rotate([90, 0, 0])
                cube([66, 3, 3], center=true);
                translate([0, -25.5, 13])
                rotate([90, 0, 0])
                cube([66, 10, 4], center=true);
                rotate([90, 0, 90])
                translate([0, 14.5, -34.5])
                cube([55, 3, 3], center=true);
                rotate([90, 0, 90])
                translate([0, 14.5, 34.5])
                cube([55, 3, 3], center=true);
                hull()
                {
                    translate([0, -diameter/2-10, 6])
                    cylinder(h=height/2, d=38, center=true);  
                    translate([0, -25.5, 6.8])
                    cube([66, 4, 3], center=true);
                }
                translate([0, -55, 7])
                scale([1, 1, 0.35])
                sphere(14);
                translate([-15, -29.5, 9.7])
                rotate([0, 90, 0])
                cylinder(h=height*1.2, d=15, $fn=5, center=true);
                translate([15, -29.5, 9.7])
                rotate([0, 90, 0])
                cylinder(h=height*1.2, d=15, $fn=5, center=true);                
            }
            union()
            {
                translate([-25,-30,height*2])
                cylinder(h=height*3, d=2, center=true);
                translate([25,-30,height*2])
                cylinder(h=height*3, d=2, center=true);
                translate([0,-74,height*2])
                cylinder(h=height*3, d=2, center=true); 
                rotate([90, 0, 90])
                translate([7, 13.5, 34.5])
                cube([12, 5, 4], center=true);
                translate([-20, 8.5, 17])
                cylinder(h=height*3, d=8, center=true);
                translate([0, -50, 0.2])
                cube([40, 50, 10], center=true);
                
            }
        }
    }
    translate([-33.5,-20,16])
    linear_extrude(5)
    text("TempoTapper", size=7, font="Zekton:style:Bold");
}



