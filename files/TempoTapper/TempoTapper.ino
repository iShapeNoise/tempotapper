/*
 * TempoTapper.ino
 * 
 * Receives currency from Piezo, calculate BPM and send it as MIDI message
 * 
 * 
 * Created: 21/03/2021
 * Author: Jens Meisner
 */


#include "MIDIUSB.h"
#include <TimerOne.h>
#include <EEPROM.h>

#define EEPROM_ADDRESS 0 // Where to save BPM
#define MINIMUM_BPM 400 // Used for debouncing
#define MAXIMUM_BPM 3000 // Used for debouncing
#define BEAT_BLINK 30 //Name builtin LED, which is TX led on Micro
#define TAP_BLINK 17 //Name builtin LED, which is RX led on Micro
#define ENCODER_PIN_A 5
#define ENCODER_PIN_B 3
#define MIDI_TIMING_CLOCK 0xF8
#define CLOCKS_PER_BEAT 24
#define MIDI_START 0xFA
#define MIDI_STOP 0xFC
#define BLINK_TIME 4 // How long to keep LED lit in CLOCK counts (so range is [0,24])
//Tap BPM Input
#define MINIMUM_TAPS 3
#define EXIT_MARGIN 150 // If no tap after 150% of last tap interval -> measure and set

long intervalMicroSeconds;
int bpm;  // BPM in tenths of a BPM!!
long calculateIntervalMicroSecs(int bpm) {
  // Take care about overflows!
  return 60L * 1000 * 1000 * 10 / bpm / CLOCKS_PER_BEAT;
}
volatile int blinkCount = 0;
const int PIEZO_PIN = A0; // Piezo output
long threshold = 25;  //Piezo sensitivity
int stepAmount = 1;    // how many points to step up/down threshold
unsigned char enc_A;
unsigned char enc_B;
unsigned char enc_A_prev = 0;
unsigned long curTime;
unsigned long loopTime;
//tapInput values
long minimumTapInterval = 60L * 1000 * 1000 * 10 / MAXIMUM_BPM;
long maximumTapInterval = 60L * 1000 * 1000 * 10 / MINIMUM_BPM;
volatile long firstTapTime = 0;
volatile long lastTapTime = 0;
volatile long timesTapped = 0;
//Pulse per quarter note. Each beat has 24 pulses.
//Tempo is based on software inner BPM.
int ppqn = 0;
bool playing = true;
int pbpm;



void setup()
{
  Serial.begin(115200);
  //  Set MIDI baud rate:
  Serial1.begin(31250);
  //Get built-in led ready to flash the beat
  pinMode(BEAT_BLINK, OUTPUT);
  //Get built-in led ready to flash on tap
  pinMode(TAP_BLINK, OUTPUT);
  //Get encoder pin A ready
  pinMode(ENCODER_PIN_A, INPUT);
  //Get encoder pin B ready
  pinMode(ENCODER_PIN_B, INPUT);
  // Get the saved BPM value from 2 stored bytes: MSB LSB
  bpm = EEPROM.read(EEPROM_ADDRESS) << 8;
  bpm += EEPROM.read(EEPROM_ADDRESS + 1);
  if (bpm < MINIMUM_BPM || bpm > MAXIMUM_BPM) {
    bpm = 1200;
  }
  // Attach the interrupt to send the MIDI clock and start the timer
  Timer1.initialize(intervalMicroSeconds);
  Timer1.setPeriod(calculateIntervalMicroSecs(bpm));
  Timer1.attachInterrupt(sendClockPulse);
  curTime = millis();
  loopTime = curTime;
 }

void loop() 
{
  //Set time now
  long now = micros();
  curTime = millis();
  if(curTime >= (loopTime + 5)){
    // 5ms since last check of encoder = 200Hz  
    enc_A = digitalRead(ENCODER_PIN_A);    // Read encoder pins
    enc_B = digitalRead(ENCODER_PIN_B);   
    if((!enc_A) && (enc_A_prev)){
      // A has gone from high to low 
      if(enc_B) {
        // B is high so clockwise
        // increase the tapTresh, dont go over 255
        if(threshold + stepAmount <= 255)
        {
          threshold += stepAmount;  
          Serial.println(threshold);  
        }
                   
      }   
      else {
        // B is low so counter-clockwise      
        // decrease the tapTresh, dont go below 0
        if(threshold - stepAmount >= 0)
        {
          threshold -= stepAmount;   
          Serial.println(threshold);              
        }
      }
    }
    enc_A_prev = enc_A;     // Store value of A for next time    
    // set the tapTresh of pin 9:
    loopTime = curTime;  // Updates loopTime
  }
  // Read Piezo ADC value in, and convert it to a voltage
  int piezoADC = analogRead(PIEZO_PIN);
  if(piezoADC >= threshold)
  {
    // Turn led on
    digitalWrite(TAP_BLINK, LOW);
    tapInput();
  }
  else{
    // Turn led off
    digitalWrite(TAP_BLINK, HIGH);
  }
  //Update bpm, after dedicating tapped interval
  if (timesTapped > 0 && timesTapped < MINIMUM_TAPS && (now - lastTapTime) > maximumTapInterval) 
  {
  // Single taps, not enough to calculate a BPM -> ignore!
    if(pbpm == bpm)
    {
      Serial.println("Ignoring lone taps!");
    }
    else if(pbpm != bpm)
    {
      playing = true;
      pbpm = bpm;
      Serial.println("New bpm");
    }
    timesTapped = 0;
  } else if (timesTapped >= MINIMUM_TAPS) {
    long avgTapInterval = (lastTapTime - firstTapTime) / (timesTapped - 1);
    if ((now - lastTapTime) > (avgTapInterval * EXIT_MARGIN / 100)) {
      playing = false;
      bpm = 60L * 1000 * 1000 * 10 / avgTapInterval;
      updateBpm(now);  
      Serial.println(bpm);
      // Update blinkCount to make sure LED blink matches tapped beat
      blinkCount = ((now - lastTapTime) * 24 / avgTapInterval) % CLOCKS_PER_BEAT;
      timesTapped = 0;
    }
  }
}

//Count gaps between taps and get average
void tapInput() {
  long now = micros();
  if (now - lastTapTime < minimumTapInterval) {
    return; // Debounce
  }
  if (timesTapped == 0) {
    firstTapTime = now;
  }
  timesTapped++;
  lastTapTime = now;
  Serial.println("Tap!");
}

//Update bpm after average of tapInput
void updateBpm(long now) {
  // Update the timer
  long interval = calculateIntervalMicroSecs(bpm);
  Timer1.setPeriod(interval);
  // Save the BPM in 2 bytes, MSB LSB
  EEPROM.write(EEPROM_ADDRESS, bpm / 256);
  EEPROM.write(EEPROM_ADDRESS + 1, bpm % 256);
  Serial.print("Set BPM to: ");
  Serial.print(bpm / 10);
  Serial.print('.');
  Serial.println(bpm % 10);
}

void playNote() {
  noteOn(0, 0, 1);   // Channel 0, middle C, normal velocity
  MidiUSB.flush();
  delay(100);
  noteOff(0, 0, 1);  // Channel 0, middle C, normal velocity
  MidiUSB.flush();
}

//Make Beat LED blink to bpm
void sendClockPulse() {
  // Write the timing clock byte
  Serial1.write(MIDI_TIMING_CLOCK);
  blinkCount = (blinkCount + 1) % CLOCKS_PER_BEAT;
  if (blinkCount == 0) {
    // Turn led off
    digitalWrite(BEAT_BLINK, HIGH);
  }
  else if (blinkCount == BLINK_TIME) {
    // Turn led on
    digitalWrite(BEAT_BLINK, LOW);
      if(playing == true)
      {
      playNote();
      }
  }
  else
  {
    // Turn led off
    digitalWrite(BEAT_BLINK, HIGH);
  }
}

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}
